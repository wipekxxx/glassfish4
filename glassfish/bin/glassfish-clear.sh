#!/usr/bin/env bash

rm -rf $GLASSFISH_HOME/glassfish/domains/domain1/generated/*
rm -rf $GLASSFISH_HOME/glassfish/domains/domain1/osgi-cache/*
rm -rf $GLASSFISH_HOME/glassfish/domains/domain1/applications/*