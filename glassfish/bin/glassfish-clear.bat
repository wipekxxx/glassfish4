@echo off

cd ../domains/domain1/generated/
echo Y | del /s /q *
cd ../osgi-cache/
echo Y | del *
cd /s /q ../applications/
echo Y | del /s /q *
exit